# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
8.12.9 Clamp

This joint grounds all 6 degrees of freedom of a node in an arbitrary position and orientation that remains fixed.

<joint_type> ::= clamp
    <joint_arglist> ::= <node_label> ,
        [ position , ]
            { node | (Vec3) <absolute_position> } ,
        [ orientation , ]
            { node | (OrientationMatrix) <absolute_orientation_matrix> }
            
The keyword node forces the joint to use the node’s position and reference frame. Otherwise, they must
be entered in the usual way for these entities. The default value for position and orientation are the
position and the orientation of the clamped node.

Private Data

The following data are available:
    
1. "Fx" constraint reaction force in global direction 1
2. "Fy" constraint reaction force in global direction 2
229
3. "Fz" constraint reaction force in global direction 3
4. "Mx" constraint reaction moment in local direction 1
5. "My" constraint reaction moment in local direction 2
6. "Mz" constraint reaction moment in local direction 3
'''
#from FreeCAD import Units

import FreeCAD
import Draft

class Clamp:
    def __init__(self, obj, label, node):
        
        #precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
         
        #Get the node's position, so that the joint is placed here: 
        x = node.absolute_position_X
        y = node.absolute_position_Y
        z = node.absolute_position_Z
        
        #Orientation of the joint:
        yaw = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('deg'))
        pitch = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('deg'))
        roll = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('deg'))  
        
        #Give the object the ability to contain other objetcs:
        obj.addExtension("App::GroupExtensionPython")         
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","Clamp","label",1).label = label
        obj.addProperty("App::PropertyString","joint","Clamp","joint",1).joint = 'clamp'
        obj.addProperty("App::PropertyString","node label","Clamp","node label",1).node_label = node.label
        obj.addProperty("App::PropertyString","plugin variables","Clamp","plugin variables",1).plugin_variables = "none"
        
        obj.addProperty("App::PropertyDistance","absolute pin position X","Absolute pin position","absolute pin position X",1).absolute_pin_position_X = x
        obj.addProperty("App::PropertyDistance","absolute pin position Y","Absolute pin position","absolute pin position Y",1).absolute_pin_position_Y = y
        obj.addProperty("App::PropertyDistance","absolute pin position Z","Absolute pin position","absolute pin position Z",1).absolute_pin_position_Z = z
        
        #absolute pin orientation:        
        obj.addProperty("App::PropertyAngle","yaw","Absolute orientation","yaw",1).yaw = yaw    
        obj.addProperty("App::PropertyAngle","pitch","Absolute orientation","pitch",1).pitch = pitch    
        obj.addProperty("App::PropertyAngle","roll","Absolute orientation","roll",1).roll = roll         
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","Animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","Animation","frame")
        obj.frame=['global','local']        
        
        obj.addProperty("App::PropertyFloat","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = 1.0

        obj.Proxy = self
                 
        #Add the vector to visualize reaction forces
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p1 = FreeCAD.Vector(x, y, z)
        p2 = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)         
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.ViewObject.Selectable = False
        d.Label = "jf: "+ label
        
    def execute(self, fp):
        JF = FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+fp.label)[0]
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        #Get the new node´s position:
        node = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_label)[0]
        x = node.absolute_position_X
        y = node.absolute_position_Y
        z = node.absolute_position_Z
        
        JF.Start =  FreeCAD.Vector(x, y, z)
        JF.End = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)
            
        #Change the joint´s position:
        fp.absolute_pin_position_X = x
        fp.absolute_pin_position_Y = y
        fp.absolute_pin_position_Z = z
        
        FreeCAD.Console.PrintMessage("CLAMP JOINT: " +fp.label+" successful recomputation...\n")                 