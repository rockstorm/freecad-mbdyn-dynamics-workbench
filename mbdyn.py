# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
This class implements the parameters to define a MBDyn simulation. 

From the webpage https://www.sky-engin.jp/en/MBDynTutorial/chap04/chap04.html

"In this block we set various conditions related to numerical computation for the problem selected in the data block. 
Because only the initial value problem is supported at present, the syntax of the block is as follows.  

begin: initial value;
   initial time: 0.;
   final time: 1.;
   time step: 1.e-3;
   max iterations: 10;
   tolerance: 1.e-6;
end: initial value;

In this example, it is set that the numerical integration is to be carried out from time 0 to 1 with a time step 1e-3. 
Conditions such as "time step," "max iterations," and "tolerance" are usually determined considering the trade off 
between computational cost and accuracy. There are many other conditions that can be set. 
Refer to the official input manual for more information."
'''

import FreeCAD

class MBDyn:
    def __init__(self, obj):
        
        obj.addExtension("App::GroupExtensionPython")  
        obj.addProperty("App::PropertyQuantity","initial_time","MBDyn","initial_time",1).initial_time = 0.0
        obj.initial_time = FreeCAD.Units.Unit('s') 
        
        obj.addProperty("App::PropertyQuantity","final_time","MBDyn","final_time").final_time = 6.0
        obj.final_time = FreeCAD.Units.Unit('s')        
        
        obj.addProperty("App::PropertyQuantity","time_step","MBDyn","time_step").time_step = 1.0e-2
        obj.time_step = FreeCAD.Units.Unit('s') 
        
        obj.addProperty("App::PropertyInteger","max_iterations","MBDyn","max_iterations").max_iterations = 100
                
        obj.addProperty("App::PropertyFloat","tolerance","MBDyn","tolerance").tolerance = 1.0e-6
        
        #obj.addProperty("App::PropertyInteger","precision","MBDyn","precision").precision = 20
        
        obj.addProperty("App::PropertyFloat","derivatives_tolerance","MBDyn","derivatives_tolerance").derivatives_tolerance = 1.e-4
        
        obj.addProperty("App::PropertyInteger","derivatives_max_iterations","MBDyn","derivatives_max_iterations").derivatives_max_iterations = 100
        
        obj.addProperty("App::PropertyString","derivatives_coefficient","MBDyn","derivatives_coefficient").derivatives_coefficient = 'auto'
        
        obj.addProperty("App::PropertyEnumeration","units","MBDyn","units")
        obj.units=['MMKS (mm-kg-sec-N-rad-C-W-J)',
                   'MKS (m-kg-sec-N-rad-C-W-J)',
                   'CGS (cm-g-sec-dyne-rad-C-W-J)',
                   'IPS (in-lb-sec-lbf-rad-F-Btu/s-Btu)']   
        
        obj.Proxy = self
        
    def execute(self, fp):
        FreeCAD.Console.PrintMessage("MBDyn OBJECT: successuful recomputation...\n") 
        
    def onDocumentRestored(self, fp):
        fp.initial_time = FreeCAD.Units.Unit('s') 
        fp.final_time = FreeCAD.Units.Unit('s')     
        fp.time_step = FreeCAD.Units.Unit('s') 