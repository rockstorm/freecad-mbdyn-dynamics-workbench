# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
8.12.39 Revolute rotation
This joint allows the relative rotation of two nodes about a given axis, which is axis 3 in the reference
systems defined by the two orientation statements. The relative position is not constrained.



     joint: 2, #hinge label
            revolute rotation,
            1, #node 1
                position, 0.0, 0.0, 0.0, #offset relative to node 1 [m]
                orientation, 3, -299.95432920955600000000, -5.23572118844330000000, 0, 2, guess, #orientation matrix relative to node 1
            2, #node 2
                position, 0.0, 0.0, -0.10, #Offset relative to node 2 [m]
                orientation, 3, -299.95432920955600000000, -5.23572118844330000000, 0, 2, guess; #orientation matrix relative to node 2

Rationale
A revolute joint without position constraints; this joint, in conjunction with an inline joint, should be
used to constrain, for example, the two nodes of a hydraulic actuator.
'''

#from FreeCAD import Units
import FreeCAD
from sympy import Point3D, Line3D
import Draft

class Revoluterotation:
    def __init__(self, obj, label, node1, node2, referenceObject1, referenceObject2, reference1, reference2):

        x1 = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        y1 = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        z1 = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))

        x2 = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        y2 = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        z2 = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))        

        obj.addExtension("App::GroupExtensionPython")          
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","Revolute rotation","label",1).label = label        
        obj.addProperty("App::PropertyString","node 1","Revolute rotation","node 1",1).node_1 = node1.label
        obj.addProperty("App::PropertyString","node 2","Revolute rotation","node 2",1).node_2 = node2.label
        obj.addProperty("App::PropertyString","joint","Revolute rotation","joint",1).joint = 'revolute rotation'

        #Absolute pin position:                
        obj.addProperty("App::PropertyDistance","absolute_pin_position_X","Absolute pin position","absolute_pin_position_X")#.absolute_pin_position_X = x
        obj.addProperty("App::PropertyDistance","absolute_pin_position_Y","Absolute pin position","absolute_pin_position_Y")#.absolute_pin_position_Y = y
        obj.addProperty("App::PropertyDistance","absolute_pin_position_Z","Absolute pin position","absolute_pin_position_Z")#.absolute_pin_position_Z = z
        
        #Relative offset 1:          
        obj.addProperty("App::PropertyDistance","relative offset 1 X","Relative offset 1","relative offset 1 X").relative_offset_1_X = x1
        obj.addProperty("App::PropertyDistance","relative offset 1 Y","Relative offset 1","relative offset 1 Y").relative_offset_1_Y = y1
        obj.addProperty("App::PropertyDistance","relative offset 1 Z","Relative offset 1","relative offset 1 Z").relative_offset_1_Z = z1
        
        #Relative offset 2: 
        obj.addProperty("App::PropertyDistance","relative offset 2 X","Relative offset 2","relative offset 2 X").relative_offset_2_X = x2
        obj.addProperty("App::PropertyDistance","relative offset 2 Y","Relative offset 2","relative offset 2 Y").relative_offset_2_Y = y2
        obj.addProperty("App::PropertyDistance","relative offset 2 Z","Relative offset 2","relative offset 2 Z").relative_offset_2_Z = z2
            
        obj.addProperty("App::PropertyString","orientation matrix 1","Revolute rotation","orientation matrix 1").orientation_matrix_1 = "3, 0.0, 0.0, 1.0, 2, guess"
        obj.addProperty("App::PropertyString","orientation matrix 2","Revolute rotation","orientation matrix 2").orientation_matrix_2 = "3, 0.0, 0.0, 1.0, 2, guess"
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","Animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","Animation","frame")
        obj.frame=['global','local']        
        
        obj.addProperty("App::PropertyFloat","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = 1.0
        
        obj.addProperty("App::PropertyString","structural dummy","Animation","structural dummy").structural_dummy = '1'              
        
        #The references define the position and orientation of the joint:                
        obj.addProperty("App::PropertyLinkSub","reference_1","Reference 1","reference_1")
        obj.addProperty("App::PropertyEnumeration","attachment_mode_1","Reference 1","attachment mode 1")
        obj.attachment_mode_1 = ['geometry´s center of mass', 'body´s center of mass', 'arc´s center']
        
        obj.addProperty("App::PropertyLinkSub","reference_2","Reference 2","reference_2")        
        obj.addProperty("App::PropertyEnumeration","attachment_mode_2","Reference 2","attachment mode 2")
        obj.attachment_mode_2 = ['geometry´s center of mass', 'body´s center of mass', 'arc´s center']      
        
        if referenceObject1 == referenceObject2:
            obj.reference_1 = (referenceObject1, reference1.SubElementNames[0])
            obj.reference_2 = (referenceObject2, reference2.SubElementNames[1])            
        
        else:
            obj.reference_1 = (referenceObject1, reference1.SubElementNames[0])
            obj.reference_2 = (referenceObject2, reference2.SubElementNames[0]) 
        
        obj.Proxy = self
        
        
        #Add joint´s rotation axis. This axis determines the "absolute_pin_orientation_matrix" and the jont´s position:                    
        p1 = obj.reference_1[0].Shape.getElement(obj.reference_1[1][0]).CenterOfMass
        p2 = obj.reference_2[0].Shape.getElement(obj.reference_2[1][0]).CenterOfMass
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        
        #Create the rotation axis:
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ label          
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.ViewObject.DrawStyle = u"Dashed"   
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.EndArrow = True
        l.ViewObject.ArrowType = u"Arrow"
        l.ViewObject.ArrowSize = str(Llength/100)#+' mm'
                     
        #Add the vector to visualize reaction forces        
        p1 = FreeCAD.Vector((p1.x+p2.x)/2 , (p1.y+p2.y)/2, (p1.z+p2.z)/2)
        p2 = FreeCAD.Vector( p1.x+Llength.Value, p1.y+Llength.Value, p1.z+Llength.Value)  
        
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.Label = "jf: "+ label                                                  
        
    def execute(self, fp):
        
        ##############################################################################Calculate the new orientation: 
        ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s Z line
        JF = FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+fp.label)[0]
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        
        if fp.attachment_mode_1 == 'geometry´s center of mass':
            p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).CenterOfGravity
            
        if fp.attachment_mode_2 == 'geometry´s center of mass':
            p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).CenterOfGravity
        
        if fp.attachment_mode_1 == 'body´s center of mass':
            p1 = fp.reference_1[0].Shape.CenterOfGravity
            
        if fp.attachment_mode_2 == 'body´s center of mass':    
            p2 = fp.reference_2[0].Shape.CenterOfGravity
            
        if fp.attachment_mode_1 == 'arc´s center':
            p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).Curve.Center
            
        if fp.attachment_mode_2 == 'arc´s center':
            p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).Curve.Center
       
        ZZ.Start = p1
        ZZ.End = p2
 
        #Two 3D points that define the joint´s line:
        p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
        l1 = Line3D(p1, p2)#Line that defines the joint
        #generate the orientation matrix:
        zzmagnitude = (l1.direction[0]**2 + l1.direction[1]**2 + l1.direction[2]**2)**0.5 
        fp.orientation_matrix_1 = "3, " + str(l1.direction[0]/zzmagnitude) + ", " + str(l1.direction[1]/zzmagnitude) + ", " + str(l1.direction[2]/zzmagnitude) + ", " +"2, guess"                
        fp.orientation_matrix_2 = "3, " + str(l1.direction[0]/zzmagnitude) + ", " + str(l1.direction[1]/zzmagnitude) + ", " + str(l1.direction[2]/zzmagnitude) + ", " +"2, guess"                       
        
        #get and update the pin position:
        x = FreeCAD.Units.Quantity((ZZ.Start[0] + ZZ.End[0])/2,FreeCAD.Units.Unit('mm')) 
        y = FreeCAD.Units.Quantity((ZZ.Start[1] + ZZ.End[1])/2,FreeCAD.Units.Unit('mm')) 
        z = FreeCAD.Units.Quantity((ZZ.Start[2] + ZZ.End[2])/2,FreeCAD.Units.Unit('mm')) 
        
        JF.Start =  FreeCAD.Vector(x, y, z)
        JF.End = FreeCAD.Vector(x+Llength, y+Llength, z+Llength) 
        
        fp.absolute_pin_position_X = x
        fp.absolute_pin_position_Y = y
        fp.absolute_pin_position_Z = z

        FreeCAD.Console.PrintMessage("REVOLUTE ROTATION JOINT: " +fp.label+" successful recomputation...\n")